package com.test.dto;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class CombinationGenerator {
    public static Map<Character, String[]> digitToAlphabets = new HashMap<>();

    static {
        digitToAlphabets.put('2', new String[]{"A", "B", "C"});
        digitToAlphabets.put('3', new String[]{"D", "E", "F"});
        digitToAlphabets.put('4', new String[]{"G", "H", "I"});
        digitToAlphabets.put('5', new String[]{"J", "K", "L"});
        digitToAlphabets.put('6', new String[]{"M", "N", "O"});
        digitToAlphabets.put('7', new String[]{"P", "Q", "R", "S"});
        digitToAlphabets.put('8', new String[]{"T", "U", "V"});
        digitToAlphabets.put('9', new String[]{"W", "X", "Y", "Z"});
        digitToAlphabets.put('0', new String[]{});
        digitToAlphabets.put('1', new String[]{});
    }

    public static LinkedList<String> retrieveTestCombinations(String input) {
        LinkedList<String> result = new LinkedList<>();
        generateCombinations(input, 0, "", result);
        return result;
    }

    public static void generateCombinations(String input, int index, String current, LinkedList<String> result) {
        if (index == input.length()) {
            result.add(current);
            return;
        }

        char digit = input.charAt(index);

        if (digitToAlphabets.containsKey(digit)) {
            String[] possibleAlphabets = digitToAlphabets.get(digit);
            for (String alphabet : possibleAlphabets) {
                generateCombinations(input, index + 1, current + alphabet, result);
            }
        }
    }
}
