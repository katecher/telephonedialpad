package com.test.dto;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.LinkedList;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class NegativeTests {

    @DataProvider(name = "Negative scenarios: input - output")
    public static Object[][] dataProviderMethod() {
        return new Object[][]{
                {"1", "0", List.of()},
                {"2", "1", List.of()},
                {"11", "01", List.of()},
                {"14", "test", List.of()},
                {"15", "?#=", List.of()},
                {"16", "", List.of()},
                {"17", "test123", List.of()},
                {"18", "*", List.of()},
                {"19", "#", List.of()},
                {"20", "2.5f", List.of()},
                {"21", "#", List.of()},};
    }

    @Test(dataProvider = "Negative scenarios: input - output")
    public void testRetrieveCombinationsNegative(String testId, String inputValue, List<String> output) {
        System.out.println("Running test with ID: " + testId);
        LinkedList<String> result = TelephoneDialPad.retrieveCombinations(inputValue);
        assertEquals(new LinkedList<>(output), result);
    }

    @Test
    public void testRetrieveCombinationsNegativeBiggerThanMaxInt() {
        System.out.println("Running test with ID: 22");

        String biggerThanMaxIntNumber = "28474836478";
        LinkedList<String> result = TelephoneDialPad.retrieveCombinations(biggerThanMaxIntNumber);
        LinkedList<String> testResult = CombinationGenerator.retrieveTestCombinations(biggerThanMaxIntNumber);
        assertEquals(testResult, result);
    }
}
