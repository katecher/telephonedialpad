package com.test.dto;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.LinkedList;
import java.util.List;


import static org.testng.Assert.assertEquals;

public class PositiveTests {

    @DataProvider(name = "Positive scenarios: input - output")
    public static Object[][] dataProviderMethod() {
        return new Object[][]{
                {"3", "2", List.of("A", "B", "C")},
                {"4", "3", List.of("D", "E", "F")},
                {"5", "4", List.of("G", "H", "I")},
                {"6", "5", List.of("J", "K", "L")},
                {"7", "6", List.of("M", "N", "O")},
                {"8", "7", List.of("P", "Q", "R", "S")},
                {"9", "8", List.of("T", "U", "V")},
                {"10", "9", List.of("W", "X", "Y", "Z")},
                //{"12", "256", },
                {"13", "20", List.of("A", "B", "C")}
        };
    }

    @Test (dataProvider = "Positive scenarios: input - output")
    public void testRetrieveCombinationsPositive(String testId, String inputValue, List<String> output)
    {
        System.out.println("Running test with ID: " + testId);
        LinkedList<String> result = TelephoneDialPad.retrieveCombinations(inputValue);
        assertEquals(new LinkedList<>(output), result);
    }

    @Test
    public void testRetrieveCombinationsPositive256() {
        System.out.println("Running test with ID: 12");

        String inputNumber = "256";
        LinkedList<String> result = TelephoneDialPad.retrieveCombinations(inputNumber);
        LinkedList<String> testResult = CombinationGenerator.retrieveTestCombinations(inputNumber);
        assertEquals(testResult, result);
    }
}
